<?php

namespace App;

use http\Exception\InvalidArgumentException;

class Color
{
    private $red;
    private $green;
    private $blue;

    /**
     * Color constructor.
     * @param $red
     * @param $green
     * @param $blue
     */
    public function __construct($red, $green, $blue)
    {
        $this->setRed($red);
        $this->setGreen($green);
        $this->setBlue($blue);
    }

    /**
     * @return mixed
     */
    public function getRed()
    {
        return $this->red;
    }

    /**
     * @param mixed $red
     */
    private function setRed($red): void
    {
        if (!range(0, 255)) {
            throw new InvalidArgumentException('Invalid number of color');
        }
        $this->red = $red;
    }

    /**
     * @return mixed
     */
    public function getGreen()
    {
        return $this->green;
    }

    /**
     * @param mixed $green
     */
    private function setGreen($green): void
    {
        if (!range(0, 255)) {
            throw new InvalidArgumentException('Invalid number of color');
        }
        $this->green = $green;
    }

    /**
     * @return mixed
     */
    public function getBlue()
    {
        return $this->blue;
    }

    /**
     * @param mixed $blue
     */
    private function setBlue($blue): void
    {
        if (!range(0, 255)) {
            throw new InvalidArgumentException('Invalid number of color');
        }
        $this->blue = $blue;
    }

    public function equals(Color $color)
    {
        if ($this == $color) {
           return true;
        }
        false;
    }

    static function random()
    {
        $red = rand(0, 255);
        $green = rand(0, 255);
        $blue = rand(0, 255);

        return new self($red, $green, $blue);
    }

    public function mix(Color $color)
    {   echo '<hr>' . 'R ';
        echo $mixed_red = ($this->red + $color->red) / 2;
        echo '<hr>' . 'G ';
        echo $mixed_green = ($this->green + $color->green) / 2;
        echo '<hr>'. 'B ';
        echo $mixed_blue = ($this->blue + $color->blue) / 2;
    }


}